n = 3

freq = Hash.new

def init_list(n)
	(0..(n - 1)).map { | x | x }
end

class Perm
	def initialize(n)
		if n.class == Perm
			@list = n.clone_list
		else
			@list = init_list(n)
		end

		def clone_list
			Array.new(@list)
		end
	end

	def swap(a, b)
		tmp = @list[a]
		@list[a] = @list[b]
		@list[b] = tmp
	end

	def to_s
		ret = ""
		for x in @list do
			ret += (x.to_s + "\t")
		end
		ret
	end

	def size
		@list.size
	end
end

def fake_rand_int(a, b)
	for x in a..b do
		yield x
	end
end

def f(k, perm, min = 0, min_change = 0, &block)
	if (k == perm.size)
		block.call(perm)
		return
	end
	fake_rand_int(min, perm.size - 1) do
		| x |
		new_perm = Perm.new(perm)
		new_perm.swap(k, x)
		f(k + 1, new_perm, min + min_change, min_change, &block)
	end
end

def f_good(k, list, &block)
	f(k, list, k, 1, &block)
end

def f_bad(k, list, &block)
	f(k, list, 0, 0, &block)
end

counter = Proc.new { | perm |
	key = perm.to_s
	if freq.has_key?(key)
		freq[key] += 1
	else
		freq[key] = 1
	end
}

def print_freq(freq)
	for key in freq.keys
		printf("%s | %d\n", key, freq[key])
	end
end

freq = Hash.new
f_bad(0, Perm.new(n), &counter)

puts "f_bad:"
print_freq(freq)

freq = Hash.new
f_good(0, Perm.new(n), &counter)

puts "f_good:"
print_freq(freq)
